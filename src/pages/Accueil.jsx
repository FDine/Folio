import styles from "./Accueil.module.css";
import BioAccueil from "../components/BioAccueil";
import TextEffet from "../components/TextEffet";
import ExperienceAccueil from "../components/ExperienceAccueil.jsx";
export default function Accueil(props) {
  return (
    <>
      <div className={styles.Accueil}>
        <div className={styles.BioAccueil}>
          <BioAccueil changePage={props.changePage} />
        </div>
        <div className={styles.Backgroundtoggle}>
          <ExperienceAccueil />
        </div>

        <TextEffet />
      </div>
    </>
  );
}
