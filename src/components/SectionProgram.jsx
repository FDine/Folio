import React, { useState } from "react";
import styles from "./TMPages.module.css";

export default function SectionProgram(props) {
  const [visible, setVisible] = useState(false);
  const togglecontent = () => {
    setVisible(!visible);
  };

  return (
    <>
      <div>
        <div className={styles.titlepage} onClick={togglecontent}>
          Program section
        </div>
        {visible && (
          <div>
            (Programmes)
            <p1>
              -It displays all the trainees programs With a grid for all the
              programs already exist
            </p1>
            <p1>-Button to add a program</p1>
            <p1>-Button to delete a program</p1>
            <p1>-Button to edit a program</p1>
            <p1>-Button to reset the inputs</p1>
            <p1>All inputs have a checker for entered data</p1>
          </div>
        )}
      </div>
    </>
  );
}
