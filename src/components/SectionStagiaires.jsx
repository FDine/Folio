import React, { useState } from "react";
import styles from "./TMPages.module.css";

export default function SectionStagiaires(props) {
  const [visible, setVisible] = useState(false);
  const togglecontent = () => {
    setVisible(!visible);
  };

  return (
    <>
      <div>
        <div className={styles.titlepage} onClick={togglecontent}>
          Intern section
        </div>

        {visible && (
          <div>
            (Stagiaires)
            <p1>-It displays all the trainee informations </p1>
            <p1>-Button to add a trainee</p1>
            <p1>-Button to delete a trainee</p1>
            <p1>-Button to edit a trainee</p1>
            <p1>-Button to reset the inputs</p1>
            <p1>All inputs have a checker for entered data</p1>
          </div>
        )}
      </div>
    </>
  );
}
