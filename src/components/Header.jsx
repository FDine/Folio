import styles from "./Header.module.css";
import MenuToggle from "./MenuToggle.jsx";

export default function MenuNav(props) {
  return (
    <header className={styles.header}>
      <div className={styles.title}>F-folio</div>

      <nav>
        <ul>
          <li>
            <button className={styles.buttonNav} href="#" onClick={props.changePage("accueil")}>
              Home
            </button>
          </li>
          <li>
            <button className={styles.buttonNav} href="#" onClick={props.changePage("AppBureau")}>
              Desktop application
            </button>
          </li>
          <li>
            <button className={styles.buttonNav} href="#" onClick={props.changePage("website")}>
              Website
            </button>
          </li>
        </ul>
      </nav>
      <div>
        {" "}
        <MenuToggle changePage={props.changePage} className={styles.MenuIcon} />
      </div>
    </header>
  );
}
