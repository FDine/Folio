import styles from "./TextEffet.module.css";

export default function Effect() {
  var ReactRotatingText = require("react-rotating-text");

  return (
    <div className={styles.TextEffet}>
      <ReactRotatingText
        items={[
          "React",
          "Wpf",
          "Handlebars",
          "Js",
          "C#",
          "C",
          "Css",
          "Ios",
          "Android",
          "Pascal",
          "Sql",
        ]}
      />
    </div>
  );
}
