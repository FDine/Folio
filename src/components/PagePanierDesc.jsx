import styles from "./PagesDesc.module.css";
import React, { useState } from "react";
export default function Section_PagePanierDesc() {
  const [visible, setVisible] = useState(false);
  const togglecontent = () => {
    setVisible(!visible);
  };

  return (
    <>
      <div>
        <div className={styles.titlepage} onClick={togglecontent}>
          Cart page
        </div>

        {visible && (
          <div>
            (Panier)
            <p1>-Only logged in customers can access to this page</p1>
            <p1>-It displays the customer's order and the price</p1>
            <p1>Icon to remove an item from the order</p1>
            <p1>-Button to submit the order</p1>
            <p1>-Button to cancel the order</p1>
          </div>
        )}
      </div>
    </>
  );
}
