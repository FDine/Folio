import styles from "./PagesDesc.module.css";
import React, { useState } from "react";

export default function PageSignInDesc() {
  const [visible, setVisible] = useState(false);
  const togglecontent = () => {
    setVisible(!visible);
  };

  return (
    <>
      <div>
        <div className={styles.titlepage} onClick={togglecontent}>
          Sign In page
        </div>

        {visible && (
          <div>
            (Inscription)
            <p1>
              -A customer can create an account with his email address and a
              password
            </p1>
          </div>
        )}
      </div>
    </>
  );
}
