import React from "react";
import styles from "./TMPages.module.css";
import SectionConsulte from "./SectionConsulte.jsx";
import SectionProgram from "./SectionProgram.jsx";
import SectionStagiaires from "./SectionStagiaires.jsx";

export default function TMPages() {
  return (
    <div className={styles.PageDesc}>
      <SectionConsulte />
      <SectionProgram />
      <SectionStagiaires />
    </div>
  );
}
