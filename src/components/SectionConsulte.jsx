import React, { useState } from "react";
import styles from "./TMPages.module.css";

export default function SectionConsulte(props) {
  const [visible, setVisible] = useState(false);
  const togglecontent = () => {
    setVisible(!visible);
  };

  return (
    <>
      <div>
        <div className={styles.titlepage} onClick={togglecontent}>
          Consult section
        </div>

        {visible && (
          <div>
            (Consulter)
            <p1>
              -It displays all the trainees ,and their information including
              their program in a Grid
            </p1>
            <p1>
              -You can display all the trainees registered in a specific program{" "}
            </p1>
            <p1>-The Refresh button to refresh the grid</p1>
          </div>
        )}
      </div>
    </>
  );
}
