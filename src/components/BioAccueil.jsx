import React from "react";
import SeeWorks from "../components/SeeWorks";
import Cv from "../resources/Cv.pdf";
import { ImLinkedin } from "react-icons/im";
import styles from "./BioAccueil.module.css";

import { AiFillGitlab } from "react-icons/ai";
export default function BioAccueil(props) {
  return (
    <>
      <div className={styles.BioA} id={styles.divEeparate}>
        <h>
          <h9>&lt;h&gt; </h9>
          HEY MY NAME IS
          <h9> &lt;/h&gt;</h9>
        </h>
        <p>FAIDINE</p>
        <p>BENNOUCHEN</p>
        <h>
          I develop websites, desktop application, mobile application (Android,
          Ios),
        </h>
        <h> web application, DB and designer </h>
        <div id={styles.divEeparate}>
          <button href={Cv} download="CVFaidine.pdf">
            Download Cv
          </button>
        </div>
        <div className={styles.SeeWorksDiv} id={styles.divEeparate}>
          <SeeWorks changePage={props.changePage} />
        </div>
        <div changePage={props.icons} id={styles.divEeparate}>
          <button
            href="https://www.linkedin.com/in/bennouchen-faidine-161b141a0/"
            target="_blank"
            rel="noreferrer" 
          >
            {" "}
            <ImLinkedin />{" "}
          </button>
          <button href="https://gitlab.com/faidine" target="_blank"
          rel="noreferrer" 
          >
            <AiFillGitlab />
          </button>
        </div>
      </div>
    </>
  );
}
