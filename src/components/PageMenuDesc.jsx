import styles from "./PagesDesc.module.css";
import React, { useState } from "react";
export default function Section_PageMenuDesc() {
  const [visible, setVisible] = useState(false);
  const togglecontent = () => {
    setVisible(!visible);
  };

  return (
    <>
      <div>
        <div className={styles.titlepage} onClick={togglecontent}>
          Menu page
        </div>

        {visible && (
          <div>
            (Menu)
            <p1>
              -It displays all the products that the customer can order
              (Recharge from DB directly)
            </p1>
            <p1>
              -Order button (It only appears when you are connected with an
              account)
            </p1>
          </div>
        )}
      </div>
    </>
  );
}
